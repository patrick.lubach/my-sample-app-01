terraform {
  backend "gcs" {
    bucket  = "zoran-testings-terraform"
    prefix  = "state"
    credentials = "./creds/serviceaccount.json"
  }
}

provider "google" {
  project     = "zoran-testings"
  region      = "europe-west4"
  credentials = "./creds/serviceaccount.json"
}
